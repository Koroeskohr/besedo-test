package com.victorviale.besedo_test.models

import Document.{Author, Body}
import categories.Subject
import cats.data._
import cats.implicits._
import com.victorviale.besedo_test.commons
import Person.Age
import categories.ClassifiedAds.{Category, Price}
import categories.Message.Recipient

object ValidationHelpers {
  val EMAIL_REGEX = raw"(\w+)@(\w+)\.(\w+)".r
  val URL_REGEX = raw"http(s?):\/\/([\w\-]+\.)?[\w\-]+\.[a-zA-Z]+".r

  def containsEmail(str: String): Boolean =
    str match {
      case EMAIL_REGEX(_*) => true
      case _ => false
    }

  def containsUrl(str: String): Boolean =
    str match {
      case URL_REGEX(_*) => true
      case _ => false
    }

  def stringHasCharacters(str: String): Boolean =
    !str.trim.isEmpty

  def validateTextualField(str: String): Boolean =
    stringHasCharacters(str) &&
      !containsUrl(str) &&
      !containsEmail(str)
}

object Validation {
  import ValidationHelpers._

  type ValidationResult[A] = ValidatedNec[String, A]

  def fromPredicate[A](p: A => Boolean, value: A, error: String): ValidationResult[A] =
    if (p(value))
      value.validNec
    else
      error.invalidNec

  def validateOption[A, B](f: A => ValidationResult[B])(maybeA: Option[A]): ValidationResult[Option[B]] = maybeA match {
    case Some(a) => f(a).map(Some(_))
    case None => (None: Option[B]).validNec
  }

  def validateGenericId(id: String) : ValidationResult[String] =
    fromPredicate(
      commons.validateGenericId,
      id,
      "unique identifier was either empty or was not alphanumerical characters")

  def validateId(id: String): ValidationResult[Document.Id] =
    validateGenericId(id).map(Document.Id)

  // Obviously not too fond of the .get, but heh
  def validateType(tpe: String): ValidationResult[Document.Type] =
    fromPredicate(
      Document.Type.allTypes.map(_.value).contains((_: String)),
      tpe,
      "Type was not in the predefined list")
      .map(validatedType => Document.Type.allTypes.find(_.value == validatedType).get)

  def validateAuthor(author: String): ValidationResult[Author] =
    validateGenericId(author).map(Author)

  // Obviously not too fond of the .get, but heh
  def validateCategory(category: String): ValidationResult[Category] =
    fromPredicate(
      Category.allCategories.map(_.value).contains((_: String)),
      category,
      "Category was not in the predefined list")
      .map(validatedCategory => Category.allCategories.find(_.value == validatedCategory).get)


  def validateBody(body: String): ValidationResult[Body] =
    fromPredicate(
      validateTextualField,
      body,
      "Body was either empty, filled with blank space or contained an illegal entity")
    .map(Body)

  def validatePriceForCategory(category: Category, price: Float): ValidationResult[Price] =
    fromPredicate(
      (p: Float) => p > 0 && category.minPrice.value < p && p < category.maxPrice.value,
      price,
      "Price was either negative or out of defined bounds")
    .map(Price)

  // Obviously not too fond of the .get, but heh
  def validateGender(gender: String) : ValidationResult[Person.Gender] =
    fromPredicate(
      Person.allGenders.map(_.value).contains((_: String)),
      gender,
      "Gender was not in the predefined list")
      .map(validatedGender => Person.allGenders.find(_.value == validatedGender).get)

  def validateAge(age: Int): ValidationResult[Age] =
    fromPredicate(
      (_: Int) >= 18,
      age,
      "Age must be eighteen or over")
    .map(Age)

  def validateSubject(subject: String): ValidationResult[Subject] =
    fromPredicate(
      validateTextualField,
      subject,
      "Subject was either empty, filled with blank space or contained an illegal entity")
    .map(Subject)

  def validateRecipient(recipient: String): ValidationResult[Recipient] =
    validateGenericId(recipient)
    .map(Recipient)

}
