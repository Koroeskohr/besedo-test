package com.victorviale.besedo_test.models

import com.victorviale.besedo_test.commons.ADT

object Person {
  sealed abstract class Gender(val value: String) extends ADT
  final case object Male extends Gender("male")
  final case object Female extends Gender("female")

  // XXX : use macro-based tool
  val allGenders = List[Gender](Male, Female)

  case class Age(value: Int) extends AnyVal
  def validateAge(age: Age): Boolean = age.value > 0
  
}
