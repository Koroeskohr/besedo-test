package com.victorviale.besedo_test.models

import cats.data.NonEmptyList
import io.circe.Decoder
import io.circe.generic.semiauto._

case class DocumentBatch(id: DocumentBatch.Id, content: NonEmptyList[Document])

object DocumentBatch {
  case class Id(value: String) extends AnyVal

  // implicit val documentDecoder = deriveDecoder[Document]
  // implicit val batchDecoder: Decoder[Either[String, DocumentBatch]] =
  //  Decoder.forProduct2[String, NonEmptyList[Document], Either[String, DocumentBatch]]("id", "content") { (id: String, content: NonEmptyList[Document]) =>
  //    Right(DocumentBatch(Id(id), content))
  //  }
}
