package com.victorviale.besedo_test
package models
package categories

import cats.implicits._

import commons.GenericId
import Validation._

case class Message(
  id: Document.Id,
  author: Document.Author,
  body: Document.Body,
  subject: Option[Subject],
  to: Message.Recipient) extends Document with HasSubject {

  val `type`: Document.Type = Document.Type.Message
}

object Message {
  case class Recipient(value: String) extends GenericId

  def validateMessage(id: String, author: String, body: String, subject: Option[String], to: String): ValidationResult[Message] =
    (validateId(id), validateAuthor(author), validateBody(body),
      validateOption[String, Subject](validateSubject)(subject), validateRecipient(to)).mapN { (i, a, b, sub, t) =>
      Message(i, a, b, sub, t)
    }
}
