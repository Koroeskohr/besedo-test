package com.victorviale.besedo_test
package models.categories

import models.{Document, Person}
import models.Validation._

import cats.implicits._

case class OnlineDatingProfile(
  id: Document.Id,
  author: Document.Author,
  body: Document.Body,
  gender: Person.Gender,
  seeks: Person.Gender,
  age: Person.Age,
  subject: Option[Subject]) extends Document with HasSubject {
  val `type` = Document.Type.Profile
}

object OnlineDatingProfile {
  def validate(id: String, author: String, body: String, gender: String, seeks: String, age: Int, subject: Option[String]): ValidationResult[OnlineDatingProfile] =
    (validateId(id), validateAuthor(author), validateBody(body),
      validateGender(gender), validateGender(seeks), validateAge(age),
      validateOption[String, Subject](validateSubject)(subject)).mapN { (i, a, b, g, s, age, sub) =>
        OnlineDatingProfile(i, a, b, g, s, age, sub)
    }
}