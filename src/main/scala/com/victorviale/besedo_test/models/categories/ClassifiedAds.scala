package com.victorviale.besedo_test
package models
package categories

import cats.data.Validated.{Invalid, Valid}
import models.Validation._
import models.Document._
import commons.ADT
import cats.implicits._
import cats.syntax._

case class ClassifiedAds(
  id: Document.Id,
  author: Document.Author,
  category: ClassifiedAds.Category,
  body: Document.Body,
  price: ClassifiedAds.Price
) extends Document {
  val `type` = Document.Type.Classified
}

object ClassifiedAds {
  sealed abstract class Category(val value: String, val minPrice: Price, val maxPrice: Price) extends ADT
  object Category {
    final case object Entertainment extends Category("entertainment", Price(1), Price(1000))
    final case object Pets extends Category("pets", Price(500), Price(1000))
    final case object Computers extends Category("computers", Price(100), Price(3500))
    final case object Food extends Category("food", Price(10), Price(200))
    final case object Miscellaneous extends Category("miscellaneous", Price(1), Price(100))

    // XXX : I should use some macro based tool
    val allCategories = List[Category](Entertainment, Pets, Computers, Food, Miscellaneous)
  }

  case class Price(value: Float) extends AnyVal

  object Validation {
    def validateClassifiedAds(id: String, author: String, category: String, body: String, price: Float): ValidationResult[ClassifiedAds] = {
      val validate1 = (validateId(id),
        validateAuthor(author),
        validateCategory(category),
        validateBody(body)).tupled

      validate1 match {
        case Valid((i, a, c, b)) => validatePriceForCategory(c, price).map(p => ClassifiedAds(i, a, c, b, p))
        case left @ Invalid(_) => left
      }
    }

  }
}