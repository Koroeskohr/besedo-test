package com.victorviale.besedo_test.models

package object categories {
  case class Subject(value: String) extends AnyVal
  trait HasSubject {
    val subject: Option[Subject]
  }
}
