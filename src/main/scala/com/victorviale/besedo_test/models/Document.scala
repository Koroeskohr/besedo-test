package com.victorviale.besedo_test
package models

import commons.{ADT, GenericId}

trait Document {
  val id: Document.Id
  val `type`: Document.Type
  val author: Document.Author
  val body: Document.Body
}

object Document {
  sealed abstract class Type(val value: String) extends ADT
  object Type {
    final case object Classified extends Type("classified")
    final case object Profile extends Type("profile")
    final case object Message extends Type("message")

    // XXX : probably should use a macro-based thing here
    val allTypes = List[Type](Classified, Profile, Message)
  }

  case class Id(value: String) extends GenericId
  case class Author(value: String) extends GenericId
  case class Body(value: String) extends AnyVal


}