package com.victorviale.besedo_test

import cats.effect.IO
import httpServices.DocumentsService
import fs2.{Stream, StreamApp}
import org.http4s.server.blaze.BlazeBuilder

import scala.concurrent.ExecutionContext

object SnippetGirlServer extends StreamApp[IO] {
  import scala.concurrent.ExecutionContext.Implicits.global

  def stream(args: List[String], requestShutdown: IO[Unit]): Stream[IO, StreamApp.ExitCode] = ServerStream.stream
}

object ServerStream {
  def stream(implicit ec: ExecutionContext): Stream[IO, StreamApp.ExitCode] =
    for {
      conf <- Stream.eval(Config.load)
      documentsService = new DocumentsService().service
      exitCode <- BlazeBuilder[IO]
        .bindHttp(conf.server.port, conf.server.host)
        .mountService(documentsService, "/")
        .serve
    } yield exitCode

}
