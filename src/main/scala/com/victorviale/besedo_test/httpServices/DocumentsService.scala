package com.victorviale.besedo_test
package httpServices

import cats.effect.IO
import com.victorviale.besedo_test.models.DocumentBatch
import org.http4s.HttpService
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl

class DocumentsService extends Http4sDsl[IO] {
  val service: HttpService[IO] = HttpService[IO] {
    case req @ POST -> Root / "documents" =>
      for {
        // batchOrErr <- req.decodeJson[Either[String, DocumentBatch]]
        // batch <- batchOrErr
        // _ <- validateBatch(batch.content)
        res <- Ok("ok")
      } yield res

  }
}
