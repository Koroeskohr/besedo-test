package com.victorviale.besedo_test

package object commons {
  trait ADT extends Product with Serializable

  trait GenericId {
    val value: String
  }

  def validateGenericId(str: String): Boolean =
    !str.isEmpty && str.matches("^[0-9a-zA-Z]+$")
}
