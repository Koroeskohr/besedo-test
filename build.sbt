val Http4sVersion = "0.18.12"
val Specs2Version = "4.2.0"
val LogbackVersion = "1.2.3"
val FicusVersion = "1.4.3"
val FlywayVersion = "5.1.0"
val RefinedVersion = "0.9.3"
val CatsVersion = "1.4.0"
val CirceVersion = "0.10.0"


lazy val root = (project in file("."))
  .settings(
    organization := "com.victorviale",
    name := "besedo-test",
    version := "0.0.1-SNAPSHOT",
    scalaVersion := "2.12.6",
    libraryDependencies ++= Seq(
      "org.http4s"      %% "http4s-blaze-server" % Http4sVersion,
      "org.http4s"      %% "http4s-circe"        % Http4sVersion,
      "org.http4s"      %% "http4s-dsl"          % Http4sVersion,
      "io.circe"        %% "circe-core"          % CirceVersion,
      "io.circe"        %% "circe-generic"       % CirceVersion,
      "org.specs2"      %% "specs2-core"         % Specs2Version % "test",
      "ch.qos.logback"  %  "logback-classic"     % LogbackVersion,
      "com.iheart"      %% "ficus"               % FicusVersion,
      "org.flywaydb"    %  "flyway-core"         % FlywayVersion,
      "eu.timepit"      %% "refined"             % RefinedVersion,
      "org.typelevel"   %% "cats-core"           % CatsVersion
    )
  )

